package mercado;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Aluno
 */
public class MenuController implements Initializable {
    @FXML 
    ChoiceBox<String> categoriaCB;
    @FXML
    TableView<Produto> tableProdutos, tableCarrinho;
    
    @FXML
    private TableColumn<?, ?> nomePD, idPD, estoquePD, precoPD, categoriaPD, validadePD, garantiaPD, marcaPD, modeloPD, nomeCR, idCR, quantCR, precoCR, categoriaCR;
    
    @FXML
    private TextField nomeTF, codigoTF, quantidadeTF, precoTF, garantiaTF, categoriaTF, marcaTF, validadeTF, modeloTF, quantEditarTF, codVendaTF, nomeClienteTF, docClienteTF;
    Produto p;
    @FXML
    private void cadastrar() {
        
        switch (categoriaCB.getSelectionModel().getSelectedItem()){            
            case "Duravel":
                p = new ProdutoDuravel(marcaTF.getText().toUpperCase());
                break;
            case "Eletronico":
                p = new ProdutoEletronico(garantiaTF.getText().toUpperCase(), marcaTF.getText().toUpperCase(), modeloTF.getText().toUpperCase());
                break;
            case "Hortifruti":
                p = new ProdutoHortifruti(getValidade());
                break;
            case "Industrializado":
                p = new ProdutoIndustrializado(marcaTF.getText().toUpperCase(), getValidade());
                break;
            
            
        }
        p.setNome(nomeTF.getText().toUpperCase());
        p.setIDProd(Integer.parseInt(codigoTF.getText()));
        p.setPreco(Double.parseDouble(precoTF.getText()));
        p.setEstoque(Double.parseDouble(quantidadeTF.getText()));
        p.insert();
        tableProdutos.refresh();
        
    }
    private Date getValidade(){
        String validade = validadeTF.getText();
        return Date.valueOf(validade.substring(6, 10)+"-"+validade.substring(3,5)+"-"+validade.substring(0,2));
    }

    @FXML
    private void adicionarCompra(){
        if(codVendaTF.getText().isEmpty()){
            //Venda v = new Venda(nomeClienteTF.getText(), docClienteTF.getText());
        }
        else{
            p.insertProdutoVenda();
        }
        
    }
    @FXML
    private void editarQuantidade() {
        p.editarEstoque(Double.parseDouble(quantEditarTF.getText()));
        tableProdutos.refresh();
    }
         
    @FXML
    private void selecionar() {
        p = tableProdutos.getSelectionModel().getSelectedItem();
    }

    private void add(){
        for (Produto pd : Produto.getAll()) {
            tableProdutos.getItems().add(pd);            
        }
    }
    private void makeColumns(){
        nomePD.setCellValueFactory(new PropertyValueFactory<>("nome"));
        idPD.setCellValueFactory(new PropertyValueFactory<>("IDProd"));
        estoquePD.setCellValueFactory(new PropertyValueFactory<>("estoque"));    
        precoPD.setCellValueFactory(new PropertyValueFactory<>("preco"));
        categoriaPD.setCellValueFactory(new PropertyValueFactory<>("categoria"));        
        validadePD.setCellValueFactory(new PropertyValueFactory<>("validade"));    
        garantiaPD.setCellValueFactory(new PropertyValueFactory<>("garantia"));
        marcaPD.setCellValueFactory(new PropertyValueFactory<>("marca"));
        modeloPD.setCellValueFactory(new PropertyValueFactory<>("modelo"));
        
        nomeCR.setCellValueFactory(new PropertyValueFactory<>("nome"));
        idCR.setCellValueFactory(new PropertyValueFactory<>("IDProd"));
        quantCR.setCellValueFactory(new PropertyValueFactory<>("estoque"));    
        precoCR.setCellValueFactory(new PropertyValueFactory<>("preco"));
        categoriaCR.setCellValueFactory(new PropertyValueFactory<>("categoria"));
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        makeColumns();
        add();
        categoriaCB.setItems(FXCollections.observableArrayList("Duravel", "Eletronico", "Hortifruti", "Industrializado"));
    }    
    
}
