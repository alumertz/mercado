package mercado;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Ana
 */
public abstract class Produto {
    private int idProd;
    private String nome;
    private double estoque;
    private double preco;
    public Produto(){
        
    }
    
    public boolean insert(){
        PreparedStatement pStatement = null;
        
        Conexao c = new Conexao("DBDS3_23", "al170302ls");
        Connection dbConnection= c.getConexao();
        
        String insertSQL = "INSERT INTO Produto(idProduto,nome, estoque, preco,categoria,validade,garantia,marca, modelo) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        try{
            pStatement = dbConnection.prepareStatement(insertSQL);
            
            pStatement.setInt(1,this.idProd);
            pStatement.setString(2,this.nome);
            pStatement.setDouble(3,this.estoque);
            pStatement.setDouble(4,this.preco);
            pStatement.setDate(6,null);
            pStatement.setString(7,null);
            pStatement.setString(8, null);
            pStatement.setString(9, null);

            if (this instanceof ProdutoDuravel){
                pStatement.setString(5,"Durável");
                pStatement.setString (8,((ProdutoDuravel) this).getMarca());
            }
            if (this instanceof ProdutoEletronico){
                pStatement.setString(5,"Eletronico");
                pStatement.setString (7,((ProdutoEletronico) this).getGarantia());
                pStatement.setString (8,((ProdutoEletronico) this).getMarca());
                pStatement.setString (9,((ProdutoEletronico) this).getModelo());
                
            }
            if (this instanceof ProdutoGarantia){
                pStatement.setString(5,"Garantia");
                pStatement.setString (7,((ProdutoGarantia) this).getGarantia());
                pStatement.setString (8,((ProdutoGarantia) this).getMarca());
            }
            if (this instanceof ProdutoHortifruti){
                pStatement.setString(5,"Hortifruti");
                pStatement.setDate(6,((ProdutoHortifruti) this).getValidade());
            }
            if (this instanceof ProdutoIndustrializado){
                pStatement.setString(5,"Industrializado");
                pStatement.setString (8, ((ProdutoIndustrializado) this).getMarca());
                pStatement.setDate(6,((ProdutoIndustrializado) this).getValidade());
            }
            
              
            pStatement.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERRRO NO BANCO");
            return false;
        }
        return true;
    }
    public void insertVenda(){
       PreparedStatement pStatement = null;
        
        Conexao c = new Conexao("DBDS3_23", "al170302ls");
        Connection dbConnection= c.getConexao(); 
    }
    public boolean insertProdutoVenda(){
        PreparedStatement pStatement = null;
        
        Conexao c = new Conexao("DBDS3_23", "al170302ls");
        Connection dbConnection= c.getConexao();
        
        String insertSQL = "INSERT INTO produtovenda(idprodutovenda,nome, cliente, pronta) values(codvenda.nextval, ?, ?, ?)";
        
        try{
           pStatement = dbConnection.prepareStatement(insertSQL);
           
           //pStatement.setString(1,p.nome);
           //pStatement.setString(2,p.nomeCliente);
           //pStatement.setBoolean(3,false);
           
           pStatement.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERRRO NO BANCO");
            return false;
        }
        return true;
    }

    public static ArrayList<Produto> getAll(){
        String selectSQL = "SELECT * FROM produto ORDER BY nome DESC";
        ArrayList <Produto> lista = new ArrayList<>();
        Conexao c = new Conexao("DBDS3_23", "al170302ls");
        Connection dbConnection = c.getConexao();
        Statement st;
        
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery (selectSQL);
            Produto p = null;
            while (rs.next()){
                if ("DURAVEL".equals(rs.getString("categoria"))){
                    p = new ProdutoDuravel(rs.getString("marca"));
                }
                else if("ELETRONICO".equals(rs.getString("categoria"))){
                    p = new ProdutoEletronico(rs.getString("garantia"),rs.getString("marca"), rs.getString("modelo"));
                }
                else if ("GARANTIA".equals(rs.getString("categoria"))){
                    p = new ProdutoGarantia(rs.getString("garantia"),rs.getString("marca"));
                }
                else if ("HORTIFRUTI".equals(rs.getString("categoria"))){
                    p = new ProdutoHortifruti(rs.getDate("validade"));
                }
                else {
                    p = new ProdutoIndustrializado(rs.getString("marca"),rs.getDate("validade"));
                } 
                
                p.setIDProd(rs.getInt("idproduto"));
                p.setNome(rs.getString("nome"));
                p.setEstoque(rs.getDouble("estoque"));
                p.setPreco(rs.getDouble("preco"));
                lista.add(p);
            }      
        }
        catch(SQLException e){
            System.out.println("Erro aqui");
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        return lista;
    }
    public void editarEstoque( double novoEstoque){
        this.estoque = novoEstoque;
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String sql = "update produto set estoque = ? where idproduto = ?";
 
        try  {
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setDouble(1, novoEstoque);
            preparedStatement.setInt(2, this.getIDProd());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
    }
        
    public int getIDProd() {
        return idProd;
    }
    public void setIDProd(int idProd) {
        this.idProd = idProd;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public double getEstoque() {
        return estoque;
    }
    public void setEstoque(double estoque) {
        this.estoque = estoque;
    }
    public double getPreco() {
        return preco;
    }
    public void setPreco(double preco) {
        this.preco = preco;
    }
    public String getCategoria() {
        if (this instanceof ProdutoDuravel){
            return "Durável";
        }
        else if (this instanceof ProdutoEletronico){
            return "Eletronico";

        }
        else if (this instanceof ProdutoGarantia){
            return "Garantia";
        }
        else if (this instanceof ProdutoHortifruti){
            return "Hortifruti";
        } 
        else if (this instanceof ProdutoIndustrializado){
            return "Industrializado";
        }
        else return "x";
    }
    
    
}
