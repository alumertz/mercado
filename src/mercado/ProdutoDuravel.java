package mercado;

/**
 *
 * @author Ana
 */
public class ProdutoDuravel extends Produto{
    private String marca;

    public ProdutoDuravel(String marca) {
        this.marca = marca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    
}
