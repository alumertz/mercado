package mercado;

/**
 *
 * @author Ana
 */
public class ProdutoEletronico extends ProdutoGarantia{
    private String modelo;

    public ProdutoEletronico(String garantia, String marca, String modelo) {
        super(garantia,marca);
        setModelo(modelo);
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
}
