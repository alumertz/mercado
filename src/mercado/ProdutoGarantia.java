package mercado;

/**
 *
 * @author Ana
 */
public class ProdutoGarantia extends ProdutoDuravel{
    private String garantia;

    public ProdutoGarantia(String garantia, String marca) {
        super(marca);
        this.garantia = garantia;
    }
    
    public String getGarantia() {
        return garantia;
    }

    public void setGarantia(String garantia) {
        this.garantia = garantia;
    }
    
    
}
 