package mercado;

import java.sql.Date;

/**
 *
 * @author Ana
 */
public class ProdutoIndustrializado extends ProdutoPerecivel{
    private String marca;

    public ProdutoIndustrializado(String marca, Date data) {
        super(data);
        this.marca = marca;
    }   

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    
}