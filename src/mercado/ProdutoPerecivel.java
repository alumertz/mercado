package mercado;

import java.sql.Date;

/**
 *
 * @author Ana
 */
public abstract class ProdutoPerecivel extends Produto {
    private Date validade; 

    public ProdutoPerecivel(Date data) {
        setValidade(data);
    }

    public Date getValidade() {
        return validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }
    
}
