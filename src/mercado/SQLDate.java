package mercado;

import java.sql.Date;

/**
 *
 * @author Ana
 */
public class SQLDate {
     
    long millis=System.currentTimeMillis();  
    java.sql.Date date=new java.sql.Date(millis);  
    

    public SQLDate() {
    }

    public long getMillis() {
        return millis;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
     
}  


